import React from 'react';
import Scrollbar from 'material-ui-shell/lib/components/Scrollbar/Scrollbar';
import SelectableMenuList from 'material-ui-shell/lib/containers/SelectableMenuList';
import { useAuth } from 'base-shell/lib/providers/Auth';
import { useConfig } from 'base-shell/lib/providers/Config';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { useIntl } from 'react-intl';
import { useLocale } from 'base-shell/lib/providers/Locale';
import { useMenu } from 'material-ui-shell/lib/providers/Menu';
import getMenuItems from '../../config/menuItems';

const Menu = (props) => {
  const intl = useIntl();
  const history = useHistory();
  const match = useRouteMatch();
  const auth = useAuth();
  const menuContext = useMenu();
  const { toggleThis, isMiniMode, isMiniSwitchVisibility } = menuContext || {};
  const { appConfig } = useConfig();
  const { setLocale, locale = 'en' } = useLocale();

  const menuItems = getMenuItems({
    intl,
    locale,
    updateLocale: setLocale,
    menuContext,
    appConfig,
    auth,
    ...props,
  }).filter((item) => {
    return item.visible !== false;
  });

  const index = match ? match.path : '/';

  const handleChange = (event, index) => {
    if (index !== undefined) {
      toggleThis('isMobileMenuOpen', false);
    }

    if (index !== undefined && index !== Object(index)) {
      history.push(index);
    }
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
      }}>
      <Scrollbar style={{ flex: 1 }}>
        <SelectableMenuList
          key={isMiniSwitchVisibility}
          onIndexChange={handleChange}
          useMinified={isMiniMode}
          items={menuItems}
          index={index}
        />
      </Scrollbar>
    </div>
  );
};

export default Menu;
