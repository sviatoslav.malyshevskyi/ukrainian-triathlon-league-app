import React from 'react';
import './NoMatch.css';

const NoMatch = () => {
  return (
    <section>
      <h2>NO MATCH</h2>
    </section>
  );
};

export default NoMatch;
