import React from 'react';
import './PasswordResetPage.css';

const PasswordResetPage = () => {
  return (
    <section className="site-section">
      <h2>Сторінка "Змінити пароль"</h2>
    </section>
  );
};

export default PasswordResetPage;
