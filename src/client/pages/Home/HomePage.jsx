import React from 'react';
import './HomePage.css';

const HomePage = () => {
  return (
    <div className="site-section">
      <h2>Головна сторінка</h2>
    </div>
  );
};

export default HomePage;
