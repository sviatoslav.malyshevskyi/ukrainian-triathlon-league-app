import React from 'react';

const UkraineFlag = () => {
  return (
    <svg width="21" height="15" xmlns="http://www.w3.org/2000/svg">
      <defs>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="a">
          <stop stopColor="#ffffff" offset="0%" />
          <stop stopColor="#f0f0f0" offset="100%" />
        </linearGradient>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="b">
          <stop stopColor="#156dd1" offset="0%" />
          <stop stopColor="#0d5eb9" offset="100%" />
        </linearGradient>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="c">
          <stop stopColor="#ffd948" offset="0%" />
          <stop stopColor="#ffd430" offset="100%" />
        </linearGradient>
      </defs>
      <g fill="none" fillRule="evenodd">
        <path fill="url(#a)" d="M0 0h21v15H0z" />
        <path fill="url(#b)" d="M0 0h21v8H0z" />
        <path fill="url(#c)" d="M0 8h21v7H0z" />
      </g>
    </svg>
  );
};

export default UkraineFlag;
