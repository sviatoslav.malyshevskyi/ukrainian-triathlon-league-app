import React from 'react';

const RussiaFlag = () => {
  return (
    <svg width="21" height="15" xmlns="http://www.w3.org/2000/svg">
      <defs>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="a">
          <stop stopColor="#ffffff" offset="0%" />
          <stop stopColor="#f0f0f0" offset="100%" />
        </linearGradient>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="b">
          <stop stopColor="#0c47b7" offset="0%" />
          <stop stopColor="#073da4" offset="100%" />
        </linearGradient>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="c">
          <stop stopColor="#e53b35" offset="0%" />
          <stop stopColor="#d32e28" offset="100%" />
        </linearGradient>
      </defs>
      <g fill="none" fillRule="evenodd">
        <path fill="url(#a)" d="M0 0h21v15H0z" />
        <path fill="url(#b)" d="M0 5h21v5H0z" />
        <path fill="url(#c)" d="M0 10h21v5H0z" />
        <path fill="url(#a)" d="M0 0h21v5H0z" />
      </g>
    </svg>
  );
};

export default RussiaFlag;
