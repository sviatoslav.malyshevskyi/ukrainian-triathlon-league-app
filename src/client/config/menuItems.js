import React from 'react';
import LanguageIcon from '../assets/icons/LanguageIcon';
import UserIcon from '../assets/icons/UserIcon';
import ExitToApp from '../assets/icons/ExitToApp';
import allLocales from './locales';
import LockIcon from '../assets/icons/LockIcon';
import HomeIcon from '../assets/icons/HomeIcon';
import InfoIcon from '../assets/icons/InfoIcon';
import RunIcon from '../assets/icons/RunIcon';
import LocationIcon from '../assets/icons/LocationIcon';
import FeedIcon from '../assets/icons/FeedIcon';
import RunningIcon from '../assets/icons/RunningCircleIcon';
import SwimmingIcon from '../assets/icons/SwimmingIcon';
import BikeIcon from '../assets/icons/BikeIcon';
import HealthSafetyIcon from '../assets/icons/HealthSafetyIcon';
import ToolboxIcon from '../assets/icons/ToolboxIcon';
import ReportIcon from '../assets/icons/ReportIcon';
import WalletIcon from '../assets/icons/WalletIcon';
import WhistleIcon from '../assets/icons/WhistleIcon';
import PersonSearchIcon from '../assets/icons/PersonSearchIcon';
import ApprovalIcon from '../assets/icons/ApprovalIcon';
import MoneyIcon from '../assets/icons/MoneyIcon';
import TrophyIcon from '../assets/icons/TrophyIcon';
import MenuOpenIcon from '../assets/icons/MenuOpenIcon';
import ChromeReaderModeIcon from '../assets/icons/ChromeReaderModeIcon';

const getMenuItems = (props) => {
  const { intl, updateLocale, locale, menuContext, auth: authData } = props;
  const {
    toggleThis,
    isDesktop,
    isAuthMenuOpen,
    isMiniSwitchVisibility,
  } = menuContext;
  const { auth, setAuth } = authData;
  const localeItems = allLocales.map((l) => {
    return {
      value: undefined,
      visible: true,
      primaryText: intl.formatMessage({ id: l.locale }),
      onClick: () => {
        updateLocale(l.locale);
      },
      leftIcon: <LanguageIcon />,
    };
  });

  const isAuthorised = auth.isAuthenticated;

  if (isAuthMenuOpen || !isAuthorised) {
    return [
      {
        value: '/my_account',
        primaryText: intl.formatMessage({
          id: 'my_account',
          defaultMessage: 'My Account',
        }),
        leftIcon: <UserIcon />,
      },
      {
        value: '/signin',
        onClick: isAuthorised
          ? () => {
              setAuth({ isAuthorised: false });
            }
          : () => {},
        visible: true,
        primaryText: isAuthorised
          ? intl.formatMessage({ id: 'sign_out' })
          : intl.formatMessage({ id: 'sign_in' }),
        leftIcon: isAuthorised ? <ExitToApp /> : <LockIcon />,
      },
    ];
  }

  return [
    {
      value: '/',
      primaryText: intl.formatMessage({ id: 'home' }),
      leftIcon: <HomeIcon />,
    },
    { divider: true },
    {
      value: '/about',
      primaryText: intl.formatMessage({ id: 'about' }),
      leftIcon: <InfoIcon />,
    },
    { divider: true },
    {
      value: '/athletes',
      primaryText: intl.formatMessage({ id: 'athletes' }),
      leftIcon: <RunIcon />,
    },
    { divider: true },
    {
      value: '/clubs',
      primaryText: intl.formatMessage({ id: 'clubs' }),
      leftIcon: <WhistleIcon />,
    },
    { divider: true },
    {
      value: '/competitions',
      primaryText: intl.formatMessage({ id: 'competitions' }),
      leftIcon: <TrophyIcon />,
    },
    { divider: true },
    {
      value: '/contacts',
      primaryText: intl.formatMessage({ id: 'contacts' }),
      leftIcon: <LocationIcon />,
    },
    { divider: true },
    {
      value: '/news',
      primaryText: intl.formatMessage({ id: 'news' }),
      leftIcon: <FeedIcon />,
    },
    { divider: true },
    {
      value: 'sports',
      primaryText: intl.formatMessage({
        id: 'sports',
        defaultMessage: 'Sports',
      }),
      primaryTogglesNestedList: true,
      leftIcon: <RunningIcon />,
      nestedItems: [
        {
          value: '/running',
          primaryText: intl.formatMessage({
            id: 'running',
            defaultMessage: 'Running',
          }),
          leftIcon: <RunningIcon />,
        },
        {
          value: '/swimming',
          primaryText: intl.formatMessage({
            id: 'swimming',
            defaultMessage: 'Swimming',
          }),
          leftIcon: <SwimmingIcon />,
        },
        {
          value: '/cycling',
          primaryText: intl.formatMessage({
            id: 'cycling',
            defaultMessage: 'Cycling',
          }),
          leftIcon: <BikeIcon />,
        },
      ],
    },
    { divider: true },
    {
      value: 'Health & Safety',
      primaryText: intl.formatMessage({ id: 'healthsafety' }),
      leftIcon: <HealthSafetyIcon />,
    },
    { divider: true },
    {
      value: 'services',
      primaryText: intl.formatMessage({ id: 'services' }),
      leftIcon: <ToolboxIcon />,
    },
    { divider: true },
    {
      value: 'accidentReport',
      primaryText: intl.formatMessage({ id: 'accidentReport' }),
      leftIcon: <ReportIcon />,
    },
    { divider: true },
    {
      value: 'partners',
      primaryText: intl.formatMessage({ id: 'partners' }),
      leftIcon: <PersonSearchIcon />,
    },
    { divider: true },
    {
      value: 'sponsors',
      primaryText: intl.formatMessage({ id: 'sponsors' }),
      leftIcon: <WalletIcon />,
    },
    { divider: true },
    {
      value: 'pressInfo',
      primaryText: intl.formatMessage({ id: 'pressInfo' }),
      leftIcon: <ApprovalIcon />,
    },
    { divider: true },
    {
      value: 'donate',
      primaryText: intl.formatMessage({ id: 'donate' }),
      leftIcon: <MoneyIcon />,
    },
    {
      primaryText: intl.formatMessage({ id: 'language' }),
      secondaryText: intl.formatMessage({ id: locale }),
      primaryTogglesNestedList: true,
      leftIcon: <LanguageIcon />,
      nestedItems: localeItems,
    },
    {
      visible: isDesktop ? true : false,
      onClick: () => {
        toggleThis('isMiniSwitchVisibility');
      },
      primaryText: intl.formatMessage({
        id: 'menu_mini_mode',
      }),
      leftIcon: isMiniSwitchVisibility ? (
        <MenuOpenIcon />
      ) : (
        <ChromeReaderModeIcon />
      ),
    },
  ];
};

export default getMenuItems;
