const locales = [
  {
    locale: 'en',
    messages: import('./en'),
    // loadData: import(`@formatjs/intl-relativetimeformat/dist/locale-data/en`),
  },
  {
    locale: 'ru',
    messages: import('./ru'),
    // loadData: import(`@formatjs/intl-relativetimeformat/dist/locale-data/ru`),
  },
  {
    locale: 'uk',
    messages: import('./uk'),
    // loadData: import(`@formatjs/intl-relativetimeformat/dist/locale-data/uk`),
  },
];

export default locales;
