const themes = [
  {
    id: 'default',
  },
  {
    id: 'primary',
    color: '#002464',
  },
  {
    id: 'secondary',
    color: 'c58f00',
  },
  {
    id: 'secondaryAlt',
    color: 'dfbc08',
  },
  {
    id: 'gray',
    color: 'c4c4c4',
  },
];

export default themes;
