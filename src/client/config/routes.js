import React, { lazy } from 'react';
import AuthorizedRoute from 'base-shell/lib/components/AuthorizedRoute/AuthorizedRoute';
import UnauthorizedRoute from 'base-shell/lib/components/UnauthorizedRoute/UnauthorizedRoute';
import Route from 'react-router-dom/es/Route';

const MyAccount = lazy(() => import('../pages/MyAccount/MyAccountPage'));
const PasswordReset = lazy(() =>
  import('../pages/PasswordReset/PasswordResetPage'),
);
const Home = lazy(() => import('../pages/Home/HomePage'));
const AboutUs = lazy(() => import('../pages/AboutUs/AboutUsPage'));
const Athletes = lazy(() => import('../pages/Athletes/AthletesPage'));
const Clubs = lazy(() => import('../pages/Clubs/ClubsPage'));
const Competitions = lazy(() =>
  import('../pages/Competitions/CompetitionsPage'),
);
const Contacts = lazy(() => import('../pages/Contacts/ContactsPage'));
const News = lazy(() => import('../pages/News/NewsPage'));
const Sports = lazy(() => import('../pages/Sports/SportsPage'));
const Running = lazy(() => import('../pages/Running/RunningPage'));
const Swimming = lazy(() => import('../pages/Swimming/SwimmingPage'));
const Cycling = lazy(() => import('../pages/Cycling/CyclingPage'));
const HealthAndSafety = lazy(() =>
  import('../pages/HealthSafety/HealthSafetyPage'),
);
const Services = lazy(() => import('../pages/Services/ServicesPage'));
const Partners = lazy(() => import('../pages/Partners/PartnersPage'));
const Sponsors = lazy(() => import('../pages/Sponsors/SponsorsPage'));
const AccidentReport = lazy(() =>
  import('../pages/AccidentReport/AccidentReportPage'),
);
const PressInfo = lazy(() =>
  import('../pages/AccidentReport/AccidentReportPage'),
);
const Donate = lazy(() => import('../pages/Donate/DonatePage'));
const NoMatch = lazy(() => import('../pages/Errors/404/NoMatch'));

const routes = [
  <UnauthorizedRoute
    path="/password_reset"
    redirectTo="/"
    exact
    component={PasswordReset}
  />,
  <AuthorizedRoute exact path="/my_account" component={MyAccount} />,
  <Route exact path="/" component={Home} />,
  <Route exact path="/about" component={AboutUs} />,
  <Route exact path="/athletes" component={Athletes} />,
  <Route exact path="/clubs" component={Clubs} />,
  <Route exact path="/competitions" component={Competitions} />,
  <Route exact path="/contacts" component={Contacts} />,
  <Route exact path="/news" component={News} />,
  <Route exact path="/sports" component={Sports} />,
  <Route exact path="/running" component={Running} />,
  <Route exact path="/swimming" component={Swimming} />,
  <Route exact path="/cycling" component={Cycling} />,
  <Route exact path="/healthsafety" component={HealthAndSafety} />,
  <Route exact path="/services" component={Services} />,
  <Route exact path="/report" component={AccidentReport} />,
  <Route exact path="/partners" component={Partners} />,
  <Route exact path="/sponsors" component={Sponsors} />,
  <Route exact path="/pressinfo" component={PressInfo} />,
  <Route exact path="/donate" component={Donate} />,
  <Route path="*" component={NoMatch} />,
];

export default routes;
