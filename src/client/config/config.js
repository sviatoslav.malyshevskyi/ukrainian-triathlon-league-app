import { lazy } from 'react';
import locales from './locales';
import routes from './routes';
import parseLanguages from 'base-shell/lib/utils/locale';

const config = {
  auth: {
    signInURL: '/signin',
  },
  routes,
  locale: {
    locales,
    defaultLocale: parseLanguages(['en', 'uk', 'ru'], 'uk'),
    onError: (e) => {
      // console.warn(e);
    },
  },
  menu: {
    MenuContent: lazy(() => import('../components/Menu/MenuContent')),
  },
  pages: {
    Home: lazy(() => import('../pages/Home/HomePage')),
    AboutUs: lazy(() => import('../pages/AboutUs/AboutUsPage')),
    Athletes: lazy(() => import('../pages/Athletes/AthletesPage')),
    Clubs: lazy(() => import('../pages/Clubs/ClubsPage')),
    Competitions: lazy(() => import('../pages/Competitions/CompetitionsPage')),
    Contacts: lazy(() => import('../pages/Contacts/ContactsPage')),
    News: lazy(() => import('../pages/News/NewsPage')),
    Sports: lazy(() => import('../pages/Sports/SportsPage')),
    Running: lazy(() => import('../pages/Running/RunningPage')),
    Swimming: lazy(() => import('../pages/Swimming/SwimmingPage')),
    Cycling: lazy(() => import('../pages/Cycling/CyclingPage')),
    HealthAndSafety: lazy(() =>
      import('../pages/HealthSafety/HealthSafetyPage'),
    ),
    Services: lazy(() => import('../pages/Services/ServicesPage')),
    Partners: lazy(() => import('../pages/Partners/PartnersPage')),
    Sponsors: lazy(() => import('../pages/Sponsors/SponsorsPage')),
    AccidentReport: lazy(() =>
      import('../pages/AccidentReport/AccidentReportPage'),
    ),
    PressInfo: lazy(() => import('../pages/AccidentReport/AccidentReportPage')),
    Donate: lazy(() => import('../pages/Donate/DonatePage')),
  },
};

export default config;
