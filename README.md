# Ukrainian Triathlon League (UTL) App

by Sviatoslav Malyshevskyi
-

## Tech stack and info:
`Create-React-App` used to bootstrap the project.<br/>
`Material-UI` used for design consistency and access to icons.<br/>
`Axios` - <br/>
`Redux` - <br/>
`Yarn` - used as the primary package download manager.<br/>
`Node.js` - <br/>
`Spring.js` - used to enable dynamic animation.


#### *Available Scripts*

`yarn start` - to run the app in the development mode on [http://localhost:3000](http://localhost:3000)

`yarn test` - to launch available tests.

`yarn build` - to assemble an optimised version of the app for the production. Available in the
 `build` folder. *The build is minified, and the filenames include the hashes.*
